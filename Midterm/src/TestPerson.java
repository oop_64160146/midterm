public class TestPerson {
    public static void main(String[] args) {
        Person Valma = new Person("Valma", "New York", 15);
        Person Dephny = new Person("Dephny", "Venice", 19);
        Person Shaggy = new Person("Shaggy", "Vengitice", 20);
        Valma.print();
        Valma.Study("mathematics");
        Valma.eat("PIZZA");

        Dephny.print();
        Dephny.eat("MAMA");
        Dephny.play("football");

        Shaggy.print();
        Shaggy.Sleep("morning");
    }
}