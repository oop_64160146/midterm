public class Car {
    private double speed = 0;
    private String model;
    private String color;
    private String brand;
    private int price;
    

    public Car(String model, String color, String brand, int price) {
        this.model = model;
        this.color = color;
        this.brand = brand;
        this.price = price;
    }

    public void print() {
        System.out.println("Model:" + model);
        System.out.println("Color:" + color);
        System.out.println("Brand:" + brand);
        System.out.println("Price:" + price);
    }

    void speed(double speed) {
        this.speed = speed;

    }

    void getSpeed() {
        System.out.println(model + " " +  "Car's speed is" + " " + speed + " " + "Km/hr");
    }
}