public class TestCar {
    public static void main(String[] args) {
        Car Ertiga = new Car("Ertiga", "Mehroon", "Maruti", 1200000);
        Car XUV500 = new Car("XUV500", "black", "Mahindra", 1500000);
        Car Swift = new Car("Swift", "red", "Maruti", 700000);
        Ertiga.print();
        XUV500.print();
        XUV500.speed(500);
        XUV500.getSpeed();
        Swift.print();
        Swift.speed(100);
        Swift.getSpeed();
    }
}
