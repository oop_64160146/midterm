public class Person {
    private String name;
    private String city;
    private int age;

    public Person(String name, String city, int age) {
        this.name = name;
        this.city = city;
        this.age = age;

    }

    public void print() {
        System.out.println("Name:" + name);
        System.out.println("City:" + city);
        System.out.println("Age:" + age);

    }

    void eat(String food) {
        System.out.println("I am eating" + " " + food);
    }

    void Sleep(String time) {
        System.out.println("I am sleeping" + " " + "at" + " " + time);
    }

    void Study(String subject) {
        System.out.println("I am studying" + " " + subject);
    }

    void play(String activity) {
        System.out.println("I am playing" + " " + activity);
    }
}